function Diagram(canvas){

  var Me = this;

  window.myCanvas = canvas;
  window.myCanvas.width = window.innerWidth;
  window.myCanvas.height = window.innerHeight;
  window.ctx = myCanvas.getContext("2d");

  myCanvas.onmousedown = function(event) {
    Me.MoveDown(event);
  };

  myCanvas.onmouseup = function(event) {
    Me.MoveUp(event);
	RefreshSelectedElement();
  };

  myCanvas.onmousemove = function(event) {
    Me.MoveMove(event);
  };
  
  myCanvas.ondblclick = function(event) {
    Me.DoubleClick(event.clientX,event.clientY);
  };
  
  myCanvas.onclick = function(event) {
    Me.Click(event.clientX,event.clientY);
  };

  document.body.onresize = function(event) {
    Me.FormChanged(window.myCanvas.offsetWidth,window.myCanvas.offsetHeight);
  };

  this.Name = 'Diagram';
  this.degV = 0;
  this.degH = 0;
  this.radius = 1;
  this.mCentre = new Point(window.innerWidth / 2, window.innerHeight / 2);
  this.Offset = new Point(0, 0);
  this.ElementList = [];
  this.XAxis = new Vector(300, 0, 0, Me.mCentre)
  this.YAxis = new Vector(0, 300, 0, Me.mCentre)
  this.ZAxis = new Vector(0, 0, 300, Me.mCentre)
  this.MouseIsDown = false;
  this.RightMouseIsDown= false;
  this.MousePosition = new Point(500, 500);
  this.ShowAxis = true;
  this.ShowPerspective = true;
  this.Mag = 1;
  this.Enabled = true;
  this.BackColor = '#ffffff';
  this.SelectedElement = null;

  this.Centre = function() {
    return new Point(Me.mCentre.X + Me.Offset.X, Me.mCentre.Y + Me.Offset.Y);
  }

  this.AddElement = function(){
      var ElementP = new Element();
	  
	  ElementP.NewElement(Me, 0, 0, 0, 0, 0, 0, 100, 100, 100, '#DDDDDD', null);

      Me.ElementList.push(ElementP);
	  
	  Me.Draw();
  }
  
  this.RemoveElement = function(elem){
	var tempList = [];
	Me.ElementList.forEach(function(Element){
		if(!Element.Compare(elem)){
			tempList.push(Element);
		}
	});
	Me.ElementList = tempList;
	Me.Draw();
  }

  this.Reset = function(){

    Me.ElementList.forEach(function(Element){
      Element.ResetVectors();
    });

    Me.XAxis = new Vector(300, 0, 0, Me.Centre());
    Me.YAxis = new Vector(0, 300, 0, Me.Centre());
    Me.ZAxis = new Vector(0, 0, 300, Me.Centre());
    Me.XAxis.mag = Me.Mag;
    Me.YAxis.mag = Me.Mag;
    Me.ZAxis.mag = Me.Mag;
    Me.Move();

  }

  this.Save = function(){
    
        var str = "";
        
        Me.ElementList.forEach(function(Element){
          str += Element.GetSaveString() + "\r\n";
        });

		//var a = document.getElementById("a");
		var a = document.createElement('a');
		
		  var file = new Blob([str], {type: 'text'});
		  a.href = URL.createObjectURL(file);
		  a.download = Me.Name + '.txt';
		  a.click();

  }

  this.Load = function(lines){
    
    Me.ElementList = [];

    lines.forEach(function(line){

	  var params = line.split("|");
	  
	  var ElementP = new Element();
      ElementP.NewElement(Me, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10]);

      Me.ElementList.push(ElementP);

    });

    Me.Draw();
  }
  
  this.GetUnderControlElement = function(){
	  var e = null;
	  Me.ElementList.forEach(function(elem){
		  if (elem.UnderControl){
			  e = elem;
		  }
	  });
	  return e;
  }

  this.MoveMove = function(event){

	var underControlElement = Me.GetUnderControlElement();
	if (underControlElement == null){
		if (Me.MouseIsDown){
		  Me.degH = Me.degHt + ((event.clientX - Me.MousePosition.X) * -0.005);
		  Me.degV = Me.degVt + ((event.clientY - Me.MousePosition.Y) * 0.005);
		  Me.Move();
		}
		if (Me.RightMouseIsDown){
		  Me.Offset.X = Me.OffX + event.clientX - Me.MousePosition.X;
		  Me.Offset.Y = Me.OffY + event.clientY - Me.MousePosition.Y;
		  Me.Reset();
		}
	}else{
		if (Me.MouseIsDown){
		  underControlElement.xRotation = Me.UnderControlElementDegHt + ((event.clientX - Me.MousePosition.X) * -0.2);
		  underControlElement.yRotation = Me.UnderControlElementDegVt + ((event.clientY - Me.MousePosition.Y) * 0.2);
		  underControlElement.ResetVectors();
		  Me.Draw();
		}
		if (Me.RightMouseIsDown){
		  underControlElement.x = Me.UnderControlElementX - (event.clientX - Me.MousePosition.X);
		  underControlElement.y = Me.UnderControlElementY - (event.clientY - Me.MousePosition.Y);
		  underControlElement.ResetVectors();
		  Me.Draw();
		}
	}

  } 

  this.Move = function(){

	Me.XAxis.Reset();
	Me.YAxis.Reset();
	Me.ZAxis.Reset();
	
    Me.XAxis.mag = Me.Mag;
    Me.YAxis.mag = Me.Mag;
    Me.ZAxis.mag = Me.Mag;

    Me.XAxis.moveDown(Me.degV);
    Me.XAxis.moveLeft(Me.degH);
    Me.YAxis.moveDown(Me.degV);
    Me.YAxis.moveLeft(Me.degH);
    Me.ZAxis.moveDown(Me.degV);
    Me.ZAxis.moveLeft(Me.degH);
	
	Me.ElementList.forEach(function(Element) {
      Element.Move(Me.degV, Me.degH);
    });

    Me.Draw();
  }

  this.degHt = 0;
  this.degVt = 0;
  this.OffX = 0;
  this.OffY = 0;
  this.UnderControlElementX = 0;
  this.UnderControlElementY = 0;
  this.UnderControlElementDegHt = 0;
  this.UnderControlElementDegVt = 0;

  this.MoveDown = function(event){

    Me.MousePosition = new Point(event.clientX,event.clientY);
	
	if (event.button == 0){
	  Me.RightMouseIsDown = true;
	}else{
	  Me.MouseIsDown = true;
	}

	var underControlElement = Me.GetUnderControlElement();
	if (underControlElement != null){
		underControlElement.LastMouseClick = event.button;
		if (event.button == 0){
		  Me.UnderControlElementX = underControlElement.x
		  Me.UnderControlElementY = underControlElement.y
		}else{
		  Me.UnderControlElementDegHt = underControlElement.xRotation;
		  Me.UnderControlElementDegVt = underControlElement.yRotation;
		}
	}else{
		if (event.button == 0){
		  Me.OffX = Me.Offset.X
		  Me.OffY = Me.Offset.Y
		}else{
		  Me.degHt = Me.degH
		  Me.degVt = Me.degV
		}
	}


  }

  this.MoveUp = function(event) {
    Me.MouseIsDown = false;
    Me.RightMouseIsDown = false;
  }

  this.Draw = function(){

    ClearGraphics(Me.BackColor);
	/*
    Me.ElementList.forEach(function(Element) {
      Element.Draw(Me.ShowPerspective);
    });*/
	
	var SortedElements = Me.ElementList.sort((a, b) => (a.AZ > b.AZ) ? 1 : -1);
	
	SortedElements.forEach(function(Element) {
      Element.Draw(Me.ShowPerspective);
    });
	
	if (Me.ShowAxis){
      DrawLine(new Point(Me.Centre().X, Me.Centre().Y), Me.XAxis.getPoint(), 1, 'rgba(231, 76, 60, 0.3)');
      DrawLine(new Point(Me.Centre().X, Me.Centre().Y), Me.YAxis.getPoint(), 1, 'rgba(231, 76, 60, 0.3)');
      DrawLine(new Point(Me.Centre().X, Me.Centre().Y), Me.ZAxis.getPoint(), 1, 'rgba(231, 76, 60, 0.3)');

      DrawString('X',  (12 * Me.Mag) + 'px Arial', '#e74c3c', Me.XAxis.getPoint());
      DrawString('Y',  (12 * Me.Mag) + 'px Arial', '#e74c3c', Me.YAxis.getPoint());
      DrawString('Z',  (12 * Me.Mag) + 'px Arial', '#e74c3c', Me.ZAxis.getPoint());
    }
	
	if (Me.SelectedElement != null){
		Me.SelectedElement.DrawOutline('#e67e22');
	}
	
	var underControlElement = Me.GetUnderControlElement();
	if (underControlElement != null){
		underControlElement.DrawOutline('#e74c3c');
	}

  }

  this.FormChanged = function(Width, Height){
    Me.mCentre = new Point(Width / 2, Height / 2);
    Me.Reset();
  };

  this.Zoom = function(perc) {
    Me.Mag = perc / 100;
    Me.ElementList.forEach(function(Element) {
      Element.Zoom();
    });
    Me.XAxis.mag = Me.Mag;
    Me.YAxis.mag = Me.Mag;
    Me.ZAxis.mag = Me.Mag;
    Me.Draw();
  };

  this.Enable = function(){
    Me.Enabled = true;
    Me.ElementList.forEach(function(Element) {
      Element.Vectorise = false;
    });
  };

  this.Disable = function(){
    Me.Enabled = false;
  }
  
  this.DoubleClick = function(x, y){
	var done = false;
	Me.ElementList.forEach(function(Element) {
      if (!done && Element.IsClicked(x,y)){
		  Element.UnderControl = !Element.UnderControl;
		  done = true;
	  }
    });
	Me.Draw();
	UpdateUI();
  }
  
  this.Click = function(x, y){
	var done = false;
	Me.ElementList.forEach(function(Element) {
      if (!done && Element.IsClicked(x,y)){
		  if (Me.SelectedElement != null && (Element.GetSaveString() == Me.SelectedElement.GetSaveString())){
			  Me.SelectedElement = null;
		  }else{
			  Me.SelectedElement = Element;
		  }
		  done = true;
	  }
    });
	Me.Draw();
	UpdateUI();
  }

}
