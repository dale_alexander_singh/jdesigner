function Element(){

  this.NewElement = function(Diagram, x, y, z, xRotation, yRotation, zRotation, height, squareWidth, squareHeight, fColour, pList){
	Me.Diagram = Diagram;
	Me.x = parseFloat(x);
	Me.y = parseFloat(y);
	Me.z = parseFloat(z);
	Me.xRotation = parseFloat(xRotation);
	Me.yRotation = parseFloat(yRotation);
	Me.zRotation = parseFloat(zRotation);
	Me.Height = parseFloat(height);
	Me.FillColour = fColour;
	Me.SquareWidth = parseFloat(squareWidth);
	Me.SquareHeight = parseFloat(squareHeight);
	
	if (pList != null && pList.length > 0){
		var points = pList.split("@");
	  
		points.forEach(function(point){
			var p = point.split(".");
			Me.AddPoint(p[0], p[1]);
		});
	}
	Me.ResetVectors();
	
	Me.Reset();
  }

  var Me = this;

  Me.Visible = true;
  Me.Diagram;
  Me.FillColour = '#888888';
  Me.Height = parseFloat(0);
  Me.PointList = [];
  Me.VectorListFront = [];
  Me.VectorListBack = [];
  Me.x = 0;
  Me.y = 0;
  Me.z = 0;
  Me.xRotation = 0;
  Me.yRotation = 0;
  Me.zRotation = 0;
  Me.SquareWidth = 0;
  Me.SquareHeight = 0;

  Me.mDown = 0;
  Me.mLeft = 0;
  Me.LastMouseClick = 0;
  
  Me.UnderControl = false;
  Me.PanelList = [];
  
  this.SetUnderControl = function(val){
	  Me.Diagram.ElementList.forEach(function(elem){
		  elem.UnderControl = false;
	  });
	  Me.UnderControl = val;
  }
  
  this.AddPoint = function(x,y){
	  x = parseInt(x);
	  y = parseInt(y);
	  
	  Me.PointList.push(new Point(x,y));
	  if (Me.SquareHeight == 0 && Me.SquareWidth == 0){
		Me.AddVectorPoint(x,y);
	  }
  }
  
  this.AddVectorPoint = function(x,y){
	  
	  var centerP = new Point(0, 0);
	  
	  var v1Prep = new Vector(x, y, (Me.Height / 2), centerP);
	  var v2Prep = new Vector(x, y, -(Me.Height / 2), centerP);
	  
	  v1Prep.RotateX(Me.xRotation);
	  v2Prep.RotateX(Me.xRotation);
	  v1Prep.RotateY(Me.yRotation);
	  v2Prep.RotateY(Me.yRotation);
	  v1Prep.RotateZ(Me.zRotation);
	  v2Prep.RotateZ(Me.zRotation);
	  
	  var v1 = new Vector(v1Prep.Ax + Me.x, v1Prep.Ay + Me.y, v1Prep.Az + Me.z, Me.Diagram.Centre());
	  var v2 = new Vector(v2Prep.Ax + Me.x, v2Prep.Ay + Me.y, v2Prep.Az + Me.z, Me.Diagram.Centre());
	  
	  Me.VectorListFront.push(v1);
	  Me.VectorListBack.push(v2);
	  
  }
  
  this.ResetVectors = function(){
	  
	Me.VectorListFront = [];
	Me.VectorListBack = [];
 
	if (Me.SquareHeight > 0 && Me.SquareWidth > 0){
		var startingX = -parseInt(Me.SquareWidth / 2)
		var startingY = -parseInt(Me.SquareHeight / 2)
		Me.AddVectorPoint(startingX,startingY);
		Me.AddVectorPoint(startingX + Me.SquareWidth, startingY);
		Me.AddVectorPoint(startingX + Me.SquareWidth, startingY + Me.SquareHeight);
		Me.AddVectorPoint(startingX, startingY + Me.SquareHeight);
	}else{
		Me.PointList.forEach(function(p){
			Me.AddVectorPoint(p.X,p.Y);
		});
	}
	
	Me.Reset();
	Me.Zoom();
	
	Me.Move(Me.mDown,Me.mLeft);
  }

  this.Zoom = function(){

	Me.VectorListFront.forEach(function(v){
		v.mag = Me.Diagram.Mag;
	});
	
	Me.VectorListBack.forEach(function(v){
		v.mag = Me.Diagram.Mag;
	});
	
	
  }

  this.Reset = function(){
	
    Me.VectorListFront.forEach(function(v){
		v.Reset();
	});
	
	Me.VectorListBack.forEach(function(v){
		v.Reset();
	});

  }

  this.AZ = 0;

  this.Draw = function(showPerspective){

	if (!Me.Visible){
		return;
	}
  
	Me.PanelList = [];
	var DiagramCentre = Me.Diagram.Centre();
  
    var PanelFront = new Panel(Me.FillColour);
	Me.VectorListFront.forEach(function(v){
		var p;
		if (showPerspective){
			p = v.getPerspectivePoint();
		}else{
			p = v.getPoint();
		}
		PanelFront.AddPoint(p.X, p.Y, v.Az);
	});
	Me.PanelList.push(PanelFront);
	
	var PanelBack = new Panel(Me.FillColour);
	Me.VectorListBack.forEach(function(v){
		var p;
		if (showPerspective){
			p = v.getPerspectivePoint();
		}else{
			p = v.getPoint();
		}
		PanelBack.AddPoint(p.X, p.Y, v.Az);
	});
	Me.PanelList.push(PanelBack);
	
	if (Me.Height > 0){
		for(i = 0; i < Me.VectorListFront.length; i++){
			var panel = new Panel(Me.FillColour);
			var a = i - 1;
			if (i == 0){
				a = Me.VectorListFront.length - 1;
			}
			panel.AddVector(Me.VectorListFront[a], showPerspective);
			panel.AddVector(Me.VectorListFront[i], showPerspective);
			panel.AddVector(Me.VectorListBack[i], showPerspective);
			panel.AddVector(Me.VectorListBack[a], showPerspective);
			Me.PanelList.push(panel);
		}
	}
	
	var SortedPanels = Me.PanelList.sort((a, b) => (a.AverageZ > b.AverageZ) ? 1 : -1);
	
	var totalZ = 0;
	
	SortedPanels.forEach(function(panel){
		totalZ = totalZ + panel.AverageZ;
		FillPolygon(panel.PointList, panel.BaseColour);
		for(i = 0;i<panel.PointList.length;i++){
			var a = i - 1;
			if (a<0){a = panel.PointList.length - 1;}
			var p1 = panel.PointList[i];
			var p2 = panel.PointList[a];
			
			DrawLine(p1,p2,0.2,'#000000');
		}
	});
	
	Me.AZ = totalZ / SortedPanels.length;
	
  }
  
  this.DrawOutline = function(colour){
	Me.PanelList.forEach(function(panel){
		for(i = 0;i<panel.PointList.length;i++){
			var a = i - 1;
			if (a<0){a = panel.PointList.length - 1;}
			var p1 = panel.PointList[i];
			var p2 = panel.PointList[a];
			DrawLine(p1,p2,1,colour);
		}
	});
  }
  
  this.IsClicked = function(x,y){
	var minX = Number.MAX_VALUE;
	var minY = Number.MAX_VALUE;
	var maxX = Number.MIN_VALUE;
	var maxY = Number.MIN_VALUE;
	Me.PanelList.forEach(function(panel){
		panel.PointList.forEach(function(pt){
			if (pt.X > maxX){ maxX = pt.X;}
			if (pt.Y > maxY){ maxY = pt.Y;}
			if (pt.X < minX){ minX = pt.X;}
			if (pt.Y < minY){ minY = pt.Y;}
		});
	});
	if ((x > minX) && (x < maxX) && (y > minY) && (y < maxY)){
		return true;
	}else{
		return false;
	}
  }

  this.Move = function(Down, Left){

    Me.mDown = Down;
    Me.mLeft = Left;
	
	Me.VectorListFront.forEach(function(v){
		v.Reset();
		v.moveDown(Me.mDown);
		v.moveLeft(Me.mLeft);
	});
	
	Me.VectorListBack.forEach(function(v){
		v.Reset();
		v.moveDown(Me.mDown);
		v.moveLeft(Me.mLeft);
	});

  }
  
  this.Compare = function(elem){
	  if (Me.GetSaveString() == elem.GetSaveString()){
		  return true;
	  }else{
		  return false;
	  }
  }

  this.GetSaveString = function(){

	  var st = "";
	  st = st + Me.x + "|";
	  st = st + Me.y + "|";
	  st = st + Me.z + "|";
	  st = st + Me.xRotation + "|";
	  st = st + Me.yRotation + "|";
	  st = st + Me.zRotation + "|";
	  st = st + Me.Height + "|";
	  st = st + Me.SquareWidth + "|";
	  st = st + Me.SquareHeight + "|";
	  st = st + Me.FillColour + "|";
	  if (Me.PointList != null){
		 for (p = 0; p < Me.PointList.length; p++){
			  var pt = Me.PointList[p];
			  if (p > 0){ st = st + "@"}
			  st = st + pt.X + "." + pt.Y;
		  } 
	  }

	  return st;
  }

}
