function Element(img, x, y, z, xRotation, yRotation, zRotation, LocalMag, DiagramMag, Centre, Path, height, lineThickness, ShowI, ShowV, fColour){

  var Me = this;

  if (ShowV == "True") {
    Me.ShowVectors = true;
  }else{
    Me.ShowVectors = false;
  }
  if (ShowI == "True") {
    Me.ShowImage = true;
  }else{
    Me.ShowImage = false;
  }
  Me.FillColour = fColour;
  Me.Height = parseFloat(height);
  Me.lineThickness = parseFloat(lineThickness);
  Me.PointListArray = [];
  Me.PointListArray.push(new PointHolder());
  Me.x = parseFloat(x);
  Me.y = parseFloat(y);
  Me.z = parseFloat(z);
  Me.LocalMag = parseFloat(LocalMag);
  Me.DiagramMag = parseFloat(DiagramMag);
  Me.xRotation = parseFloat(xRotation);
  Me.yRotation = parseFloat(yRotation);
  Me.zRotation = parseFloat(zRotation);
  Me.Centre = Centre;
  Me.ImagePath = Path;
  Me.img = img;

  Me.mRotationZ = 0;
  Me.mDeg = 0;
  Me.mDown = 0;
  Me.mLeft = 0;
  Me.rDown = 0;
  Me.rLeft = 0;
  Me.Vectorise = false;
  Me.pZoom = 1.0
  
  this.GetPanelColourPerc = function(){
	  var p1 = Me.CompassCentre.getPoint();
	  var p2 = Me.Compass.getPoint();
	  return -((p2.X - p1.X) + (p2.Y - p1.Y)) / 2.5;
  }

  this.Zoom = function(Mag, Local){
    if (Local){
      Me.LocalMag = Mag;
    }else{
      Me.DiagramMag = Mag;
    }

	//Me.Compass.mag = (Me.DiagramMag * Me.LocalMag);
	//Me.CompassCentre.mag = (Me.DiagramMag * Me.LocalMag);
    Me.TopLeft.mag = (Me.DiagramMag * Me.LocalMag);
    Me.TopRight.mag = (Me.DiagramMag * Me.LocalMag);
    Me.BottomLeft.mag = (Me.DiagramMag * Me.LocalMag);
    Me.BottomRight.mag = (Me.DiagramMag * Me.LocalMag);
   }

  this.ToString = function(){
    if (Me.ImagePath.Trim = ""){
      return "Element";
    }else{
      return "";
    }
  }

  this.Reset = function(Centre){

    Me.Centre = Centre;
    var p = new Point(0, 0);
    var wdth = Me.img.Width / 2;
    var hght = Me.img.Height / 2;
	
	Me.Compass = new Vector(0, 0, 100, p);
	Me.CompassCentre = new Vector(0, 0, 0, p);
    Me.TopLeft = new Vector(-wdth, hght, 0, p);
    Me.TopRight = new Vector(wdth, hght, 0, p);
    Me.BottomLeft = new Vector(-wdth, -hght, 0, p);
    Me.BottomRight = new Vector(wdth, -hght, 0, p);

    Me.RotateZ(Me.zRotation);
    Me.Rotate(Me.yRotation, Me.xRotation);

	Me.Compass = new Vector(Me.Compass.Ax + Me.x, Me.Compass.Ay + Me.y, Me.Compass.Az + Me.z, Me.Centre);
	Me.CompassCentre = new Vector(Me.CompassCentre.Ax + Me.x, Me.CompassCentre.Ay + Me.y, Me.CompassCentre.Az + Me.z, Me.Centre);
    Me.TopLeft = new Vector(Me.TopLeft.Ax + Me.x, Me.TopLeft.Ay + Me.y, Me.TopLeft.Az + Me.z, Me.Centre);
    Me.TopRight = new Vector(Me.TopRight.Ax + Me.x, Me.TopRight.Ay + Me.y, Me.TopRight.Az + Me.z, Me.Centre);
    Me.BottomLeft = new Vector(Me.BottomLeft.Ax + Me.x, Me.BottomLeft.Ay + Me.y, Me.BottomLeft.Az + Me.z, Me.Centre);
    Me.BottomRight = new Vector(Me.BottomRight.Ax + Me.x, Me.BottomRight.Ay + Me.y, Me.BottomRight.Az + Me.z, Me.Centre);

	//Me.Compass.mag = (Me.DiagramMag * Me.LocalMag);
	//Me.CompassCentre.mag = (Me.DiagramMag * Me.LocalMag);
    Me.TopLeft.mag = (Me.DiagramMag * Me.LocalMag);
    Me.TopRight.mag = (Me.DiagramMag * Me.LocalMag);
    Me.BottomLeft.mag = (Me.DiagramMag * Me.LocalMag);
    Me.BottomRight.mag = (Me.DiagramMag * Me.LocalMag);

  }

  this.ZA = function(){
      return (TopLeft.Az + BottomRight.Az) / 2;
  }

  this.Draw = function(colour, showPerspective){
    var Array = [];
	
	//DrawLine(Me.CompassCentre.getPoint(), Me.Compass.getPoint(), 1, '#00FF00');
	
	colour = Me.FillColour;

    if (Me.ShowVectors){

      Me.PointListArray.forEach(function(PointList){

        var pFill1 = [];
        var pFill2 = [];

        var FirstPoint = null;
        var SecondPoint = null;

        PointList.PointList.forEach(function(p){

          pFill1.push(Me.GetVectorPointFromPoint(p, (Me.Height / 2), showPerspective));
          pFill2.push(Me.GetVectorPointFromPoint(p, -(Me.Height / 2), showPerspective));
          
          SecondPoint = p;
          if (FirstPoint != null){
            var p1 = Me.GetVectorPointFromPoint(FirstPoint, -(Me.Height / 2), showPerspective);
            var p2 = Me.GetVectorPointFromPoint(SecondPoint, -(Me.Height / 2), showPerspective);
            //DrawLine(p1, p2, Me.lineThickness, lightenColor(colour, 0.4));
			//DrawLine(p1, p2, Me.lineThickness, '#000000');
          }
          FirstPoint = p;
        });

        if(PointList.Fill){
          //FillPolygon(pFill1, lightenColor(colour, 0.2));
          //FillPolygon(pFill2, lightenColor(colour, 0.2));
		  
		  
		  
		  FillPolygon(pFill1, shadeColor(colour, Me.GetPanelColourPerc()));
        }

      });

      if (Me.Height > 0){

        Me.PointListArray.forEach(function(PointList){
          var FirstPoint = null;
          var SecondPoint = null;
          PointList.PointList.forEach(function(p){
            SecondPoint = p;
            if (FirstPoint != null){
              var p1 = Me.GetVectorPointFromPoint(FirstPoint, Me.Height / 2, showPerspective);
              
              //DrawLine(p1, Me.GetVectorPointFromPoint(SecondPoint, Me.Height / 2, showPerspective), lightenColor(colour, 0.4), Me.lineThickness);
              //DrawLine(p1, Me.GetVectorPointFromPoint(FirstPoint, -(Me.Height / 2), showPerspective), lightenColor(colour, 0.4), Me.lineThickness);
			  DrawLine(p1, Me.GetVectorPointFromPoint(SecondPoint, Me.Height / 2, showPerspective), '#000000', Me.lineThickness);
			  DrawLine(p1, Me.GetVectorPointFromPoint(FirstPoint, -(Me.Height / 2), showPerspective), '#000000', Me.lineThickness);
              
              var pArray = [];

              pArray.push(Me.GetVectorPointFromPoint(FirstPoint, Me.Height / 2, showPerspective));
              pArray.push(Me.GetVectorPointFromPoint(SecondPoint, Me.Height / 2, showPerspective));
              pArray.push(Me.GetVectorPointFromPoint(SecondPoint, -(Me.Height / 2), showPerspective));
              pArray.push(Me.GetVectorPointFromPoint(FirstPoint, -(Me.Height / 2), showPerspective));

              FillPolygon(pArray, lightenColor(colour, 0.2));
            }
            FirstPoint = p;
          });
        });

      }

    }

  }

  this.GetVectorPointFromPoint = function(point, height, showPerspective){
    var v = new Vector((Me.img.Width / 2) - point.X, (Me.img.Height / 2) - point.Y, height, new Point(0, 0));
    v = Me.RotateVector(v, Me.mDeg);
    v.RotateY(Me.rDown);
    v.RotateX(Me.rLeft);
    v = new Vector(v.Ax + Me.x, v.Ay + Me.y, v.Az + Me.z, Me.Centre);
    v.moveDown(Me.mDown);
    v.moveLeft(Me.mLeft);
    v.mag = (Me.DiagramMag * Me.LocalMag);
	if(showPerspective){
		return v.getPerspectivePoint();
	}else{
		return v.getPoint();
	}
    
  }

  this.Move = function(Down, Left){

    Me.Reset(Me.Centre);
    Me.mDown = Down;
    Me.mLeft = Left;

	Me.Compass.moveDown(Down);
	Me.CompassCentre.moveDown(Down);
    Me.TopLeft.moveDown(Down);
    Me.TopRight.moveDown(Down);
    Me.BottomLeft.moveDown(Down);
    Me.BottomRight.moveDown(Down);

	Me.Compass.moveLeft(Left);
	Me.CompassCentre.moveLeft(Left);
    Me.TopLeft.moveLeft(Left);
    Me.TopRight.moveLeft(Left);
    Me.BottomLeft.moveLeft(Left);
    Me.BottomRight.moveLeft(Left);

  }

  this.Rotate = function(Down, Left){
    Me.rDown = Down;
    Me.rLeft = Left;
	
	Me.Compass.RotateY(Down);
	Me.CompassCentre.RotateY(Down);
    Me.TopLeft.RotateY(Down);
    Me.TopRight.RotateY(Down);
    Me.BottomLeft.RotateY(Down);
    Me.BottomRight.RotateY(Down);

	Me.Compass.RotateX(Left);
	Me.CompassCentre.RotateX(Left);
    Me.TopLeft.RotateX(Left);
    Me.TopRight.RotateX(Left);
    Me.BottomLeft.RotateX(Left);
    Me.BottomRight.RotateX(Left);
  }

  this.RotateZ = function(Deg){

    if (Deg.toString() != "0"){
      Me.mDeg = Deg;
	  Me.Compass = Me.RotateVector(Me.Compass, Deg);
	  Me.CompassCentre = Me.RotateVector(Me.Compass, Deg);
      Me.TopLeft = Me.RotateVector(Me.TopLeft, Deg);
      Me.TopRight = Me.RotateVector(Me.TopRight, Deg);
      Me.BottomLeft = Me.RotateVector(Me.BottomLeft, Deg);
      Me.BottomRight = Me.RotateVector(Me.BottomRight, Deg);
    }

  }

  this.RotateVector = function(vec, Deg){

      var Deg360 = (Math.PI / 180) * Deg;

      var hyp = Math.sqrt((vec.Ax * vec.Ax) + (vec.Ay * vec.Ay));

      var Theta = Math.acos(vec.Ay / hyp);

      if (vec.Ax < 0){
        Theta = Math.acos(vec.Ay / hyp);
        Theta = (2 * Math.PI) - Theta;
      }else{
        Theta = Math.acos(vec.Ay / hyp);
      }

      if(parseFloat(Theta).toString() == 'NaN'){
        if (vec.Ay > 0) {
          Theta = Math.PI;
        }else{
          Theta = 0;
        }
      }

      Theta = Theta - Deg360;

      var Ay = hyp * Math.cos(Theta);
      var Ax = hyp * Math.sin(Theta);

      return new Vector(Ax, Ay, vec.Az, Me.Centre);

  }

  /*
  Public Sub AddPoints(HostForm As PictureBox)

    Me.Vectorise = True
    Me.Form = HostForm
    Me.ShowVectors = True
    DrawSimple()

  End Sub
  

  Public Sub MouseDown() Handles Form.DoubleClick

    If Vectorise Then
      Dim p As Point = Form.PointToClient(Form.MousePosition)
      Dim Pzoom = Me.pZoom
      p = New Point(p.X / Pzoom, p.Y / Pzoom)
      Me.PointListArray(PointListArray.Count - 1).PointList.Add(p)
      Me.ShowVectors = True
      DrawSimple()
    End If

  End Sub

  Public Sub DrawSimple()

    Dim Array() As Point
    ReDim Array(2)

    Dim g As Graphics = Form.CreateGraphics()
    g.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

    g.Clear(Color.White)

    Array(0) = New Point(Me.img.Width * pZoom, 0)
    Array(1) = New Point(0, 0)
    Array(2) = New Point(Me.img.Width * pZoom, Me.img.Height * pZoom)
    g.DrawImage(Me.img, Array)

    For Each PointList In PointListArray
      Dim FirstPoint As Object = Nothing
      Dim SecondPoint As Object = Nothing
      For Each p In PointList.PointList
        p = New Point(p.X * pZoom, p.Y * pZoom)
        SecondPoint = p
        If FirstPoint IsNot Nothing Then
          g.DrawLine(Pens.Red, FirstPoint, SecondPoint)
        End If
        FirstPoint = p
      Next
    Next

  End Sub
  */

  this.AddPointList = function(){
    Me.PointListArray.push(new PointHolder());
  }

  this.ClearPoints = function(){
    Me.PointListArray = [];
    Me.PointListArray.push(new PointHolder());
  }

  Me.Reset(Me.Centre);

}
