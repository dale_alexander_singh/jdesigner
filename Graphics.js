function DrawLine(p1,p2, thickness, colour){
    window.ctx.beginPath();
    window.ctx.lineWidth = thickness;
    window.ctx.strokeStyle = colour;
    window.ctx.fillStyle = colour;
    window.ctx.moveTo(p1.X, p1.Y);
    window.ctx.lineTo(p2.X, p2.Y);
    window.ctx.closePath();
    window.ctx.stroke();
}

function FillPolygon(pList, colour){
    window.ctx.beginPath();
    window.ctx.fillStyle = colour;
    for (p = 0; p < pList.length; p++) { 
        var pt = pList[p];
        if (p == 0){
            window.ctx.moveTo(pt.X, pt.Y);
        }else{
            window.ctx.lineTo(pt.X, pt.Y);
        }
    }
    window.ctx.closePath();
    window.ctx.fill();
}

function DrawString(text, font, colour, p){
    window.ctx.font = font;
    window.ctx.fillStyle = colour;
    window.ctx.textAlign = 'center';
    window.ctx.fillText(text, p.X, p.Y);
}

function ClearGraphics(colour){
    window.ctx.strokeStyle = colour;
    window.ctx.fillStyle = colour;
    window.ctx.fillRect(0,0, window.myCanvas.width, window.myCanvas.height);
}

function lightenColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);
    var O = parseInt(255 * percent);

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));
    var OO = ((O.toString(16).length==1)?"0"+O.toString(16):O.toString(16));

    return "#"+RR+GG+BB+OO;
}

function shadeColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}

function stretchImage(srcImgElem, TopLeft, TopRight, BottomRight, BottomLeft)
{

    var imgWidth = srcImgElem.width;
    var imgHeight = srcImgElem.height;

    var split = 20;
    var splitH = imgHeight / split;

    for (h = 0; h < imgHeight; h = h + splitH){
        
        //window.ctx.drawImage(srcImgElem, TopLeft.X, h, width, 1, 0, h, width, 1);
        
        //window.ctx.drawImage(srcImgElem, TopLeft.X, h, width, 1, 0, h, w, 1);
        var perc = h/imgHeight;

        var xOneTotal = TopLeft.X - BottomLeft.X;
        var xOne = BottomLeft.X + (xOneTotal * perc);
        
        var xTwoTotal = TopRight.X - BottomRight.X;
        var xTwo = BottomRight.X + (xTwoTotal * perc);

        var yOneTotal = BottomLeft.Y - TopLeft.Y;
        var yOne = BottomLeft.Y - (yOneTotal * perc);
        
        var yTwoTotal = BottomRight.Y - TopRight.Y;
        var yTwo = BottomRight.Y - (yTwoTotal * perc);


        var pOne = new Point(xOne, yOne);
        var pTwo = new Point(xTwo, yTwo);



        //DrawLine(pOne,pTwo, 1, "rgba(0,255,0,0.3)")

        var x = xTwo - xOne;
        var y = yTwo - yOne;

        var w = Math.sqrt((x * x) + (y * y));

        var theta = w * Math.sin(y);
        theta = 10 * (Math.PI / 180);

        window.ctx.translate(xOne, yOne);
        //window.ctx.rotate(theta);
        window.ctx.translate(-xOne, -yOne);
        
        window.ctx.drawImage(srcImgElem, 
            0, h, 
            imgWidth, splitH, 
            xOne, yOne, 
            w, splitH);

        //window.ctx.rotate(-theta);

    }


    window.ctx.beginPath();
    window.ctx.lineWidth = 3;
    window.ctx.strokeStyle = 'red';
    window.ctx.fillStyle = 'red';
    window.ctx.moveTo(TopLeft.X, TopLeft.Y);
    window.ctx.lineTo(TopRight.X, TopRight.Y);
    window.ctx.lineTo(BottomRight.X, BottomRight.Y);
    window.ctx.lineTo(BottomLeft.X, BottomLeft.Y);
    window.ctx.closePath();
    window.ctx.stroke();
}

