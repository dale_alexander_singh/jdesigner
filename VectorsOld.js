﻿function Vector(xt, yt, zt, Centre) {

  this.X = 0.00;
  this.Y;
  this.Z;
  this.xLength;
  this.yLength;
  this.Length;
  this.Ax;
  this.Ay;
  this.Az;
  this.Xtheta;
  this.Ytheta;
  //this.Ori = new Point(500, 500);
  this.mag = 1;
  this.PerpectiveDistance = 1000;

  var Me = this;

  this.Reset = function () {

    Me.xLength = Math.sqrt((Me.X * Me.X) + (Me.Z * Me.Z));
    Me.yLength = Math.sqrt((Me.Z * Me.Z) + (Me.Y * Me.Y));

    Me.Ax = Me.X;
    Me.Ay = Me.Y;
    Me.Az = Me.Z;

    Me.Length = Math.sqrt((Me.X * Me.X) + (Me.Y * Me.Y) + (Me.Z * Me.Z));

    Me.Ytheta = Math.acos(Me.Z / Me.yLength);
    Me.Xtheta = Math.acos(Me.Z / Me.xLength);

  }

  //constructor

  this.Ori = Centre;

  this.X = xt;
  this.Y = yt;
  this.Z = zt;

  Me.Reset();

  //

  this.getPoint = function () {

    return new Point(Me.Ori.X - (Me.Ax * Me.mag), Me.Ori.Y - (Me.Ay * Me.mag));

  }

  this.getPerspectivePoint = function () {
    
    var thetaX = Math.atan(Me.Ax / Me.PerpectiveDistance);
    var tempX = (Me.PerpectiveDistance + Me.Az) * Math.tan(thetaX);

    var thetaY = Math.atan(Me.Ay / Me.PerpectiveDistance);
    var tempY = (Me.PerpectiveDistance + Me.Az) * Math.tan(thetaY);

    return new Point(Me.Ori.X - (tempX * Me.mag), Me.Ori.Y - (tempY * Me.mag));

  }

  this.getX = function () {

    var answer = Me.Ax;

    if (answer < 0){
      answer = Math.ceil(answer);
    }
    else {
      answer = Math.floor(answer);
    }

    return parseInt(answer);

  }

  this.getY = function () {

    var answer = Me.Ay;

    if (answer < 0) {
      answer = Math.ceil(answer);
    }
    else {
      answer = Math.floor(answer);
    }

    return parseInt(answer);

  }

  this.moveUp = function (deg) {

    Me.yLength = Math.sqrt((Me.Length * Me.Length) - (Me.Ax * Me.Ax));

    if (Me.Ay < 0) {
      Me.Ytheta = Math.acos(Me.Az / Me.yLength);
      Me.Ytheta = (2 * Math.PI) - Me.Ytheta;
    }
    else {
      Me.Ytheta = Math.acos(Me.Az / Me.yLength);
    }


    if (Me.Ytheta.toString() == NaN.toString()) {

      if (Me.Az >= 0) {
        Me.Ytheta = 0;
      }
      else {
        Me.Ytheta = Math.PI;
      }

    }

    Me.Ytheta = Me.Ytheta + deg;
    
    Me.Ay = Me.yLength * Math.sin(Me.Ytheta);
    Me.Az = Me.yLength * Math.cos(Me.Ytheta);

  }

  this.moveDown = function (deg) {

    Me.moveUp(-deg);

  }

  this.moveLeft = function (deg) {

    Me.xLength = Math.sqrt((Me.Length * Me.Length) - (Me.Ay * Me.Ay));

    if (Me.Ax < 0) {
      Me.Xtheta = Math.acos(Me.Az / Me.xLength);
      Me.Xtheta = (2 * Math.PI) - Me.Xtheta;
    }
    else {
      Me.Xtheta = Math.acos(Me.Az / Me.xLength);
    }


    if (Me.Xtheta.toString() == NaN.toString()) {

      if (Me.Az >= 0) {
        Me.Xtheta = 0;
      }
      else {
        Me.Xtheta = Math.PI;
      }

    }

    Me.Xtheta = Me.Xtheta + deg;

    Me.Ax = Me.xLength * Math.sin(Me.Xtheta);
    Me.Az = Me.xLength * Math.cos(Me.Xtheta);

  }

  this.moveRight = function (deg) {

    Me.moveLeft(-deg);

  }

  this.RotateX = function (deg) {

    var d = (Math.PI / 180) * deg;
    Me.moveLeft(d);

  }

  this.RotateY = function (deg) {

    var d = (Math.PI / 180) * deg;
    Me.moveDown(d);

  }

}