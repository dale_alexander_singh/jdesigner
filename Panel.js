function Panel(colour){
	var Me = this;
    this.PointList = [];
	this.TotalX = 0;
	this.TotalY = 0;
	this.TotalZ = 0;
	this.AverageZ = 0;
	this.BaseColour = colour;
	this.AddPoint = function(x,y,z){
		Me.PointList.push(new Point(x,y));
		Me.TotalX = Me.TotalX + x;
		Me.TotalY = Me.TotalY + y;
		Me.TotalZ = Me.TotalZ + z;
		Me.AverageZ = Me.TotalZ / Me.PointList.length;
	}
	this.AddVector = function(v, showPerspective){
		var p;
		if (showPerspective){
			p = v.getPerspectivePoint();
		}else{
			p = v.getPoint();
		}
		Me.AddPoint(p.X, p.Y, v.Az);
	}
	this.GetColour = function(p1){
		return Me.BaseColour;
		/*
		var x = Me.TotalX / Me.PointList.length;
		var y = Me.TotalY / Me.PointList.length;
		
		var p2 = new Point(x,y)
		
		var perc = -((p2.X - p1.X) + (p2.Y - p1.Y)) / 5.5;
		
		return shadeColor(Me.BaseColour, perc);
		*/
	}
}